<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     * 定义无需报告的异常类型
     *
     * @var array
     */
    protected $dontReport = [

    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     * 报告或者通过日志记录一个异常
     * 可以在这个方法中将异常报告给 Sentry,Bugsnag 等平台
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     * 将异常通过 Http 返回,将异常渲染至HTTP 响应值当中
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof \App\Exceptions\CustomException){
            return $exception->render($request);
        }
        return parent::render($request, $exception);
    }

    protected function unauthenticated($request,AuthenticationException $exception){
        if($request->expectsJson()){
            return response()->json(['error' => 'Unauthenticated.'],401);
        }
        return redirect()->guest(route('login'));
    }

}
