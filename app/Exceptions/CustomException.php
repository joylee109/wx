<?php

namespace App\Exceptions;

use Exception;

class CustomException extends Exception{
    //
    /*
     * 报告这个异常
     */
    public function report(){

    }

    /**
     * 将异常渲染至 HTTP 响应值中。
     */
    public function render($request){
        return response()->view('errors.custom',array(
            'exception' => $this
        ));
    }
}