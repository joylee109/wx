<?php
// 获取session 中的用户信息
if(!function_exists('get_user_info')){
    function get_user_info(){
        $open_id = session('wechat.oauth_user.default')->id;
        return session('user_info_' . $open_id, []);
    }
}
// 通过access_token 和open_id 获取微信用户信息
if(!function_exists('get_userinfo_from_openid')){
    function get_userinfo_from_openid($openid){
        if(!empty($openid)){
            $app = app('wechat.official_account');
            $weixin_userinfo = $app->user->get($openid);
        }else{
            $weixin_userinfo = [];
        }
        return $weixin_userinfo;
    }
}
