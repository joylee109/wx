<html>
    <head>
        <title>test 页面</title>
    </head>
    <body>
        @if(count($errors) > 0)
            <ul>
                @foreach($errors as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <ul>
            @if(session()->has('errors'))
                @foreach(session()->get('errors') as $error)
                    <li>{{ $error }}</li>
                @endforeach
            @endif
        </ul>
    </body>
</html>