<?php

namespace App\Http\Controllers\Weixin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;
use EasyWeChat\Kernel\Messages\Text;
use App\Models\User;

class IndexController extends Controller{
    public function index(){
        $app = app('wechat.official_account');
        $app->server->push(function($message){
            switch ($message['MsgType']){
                case 'event':
                    switch($message['Event']){
                        case 'subscribe':
                            $user = User::where('openid',$message['FromUserName'])->first();
                            if(is_null($user)){
                                $user_weixin_info = get_userinfo_from_openid($message['FromUserName']);
                                $user = new User;
                                $user->fill($user_weixin_info);
                                $user->avatar = $user_weixin_info['headimgurl'];
                                $user->book_coin = 0;
                                $user->password = bcrypt('weierban123456');
                                $user->is_vip = 0;
                                $user->subscribe = 1;
                                $user->save();
                            }else{
                                $user->subscribe = 1;
                                $user->save();
                            }
                            break;
                        case 'unsubscribe':
                            User::where('openid',$message['FromUserName'])->update(['subscribe'=>0]);
                            break;
                        case 'VIEW':
                            Log::info(date('Y-m-d H:i:s')."\r\n\r\n".$message['EventKey']);
                            break;
                    }
                    return '事件消息';
                    break;
                case 'text':
                    return '收到文字消息';
                    break;
                case 'image':
                    return '收到图片消息';
                    break;
                case 'voice':
                    return '收到语音消息'.$message['Recognition'];
                    break;
                case 'video':
                    return '收到视频消息';
                    break;
                case 'location':
                    return '收到坐标消息,经度为:'.$message['Location_Y']."\n".'纬度为:'.$message['Location_X'];
                    break;
                case 'link':
                    return '收到链接消息';
                    break;
                case 'file':
                    return '收到文件消息';
                // ... 其它消息
                default:
                    return '收到其它消息';
                    break;
            }
        });
        $response = $app->server->serve();
        return $response;
    }

    public function show_menu(){
        $app = app('wechat.official_account');
        $list = $app->menu->list();
        echo '<pre>';
        print_r($list);
        echo '</pre>';

    }

    public function create_menu(){
        $buttons = [
            [
                "name"       => "菜单",
                "sub_button" => [
                    [
                        "type" => "view",
                        "name" => "搜索",
                        "url"  => route('home.index.index'),
                    ],
                    [
                        "type" => "view",
                        "name" => "视频",
                        "url"  => "http://v.qq.com/"
                    ]
                ],
            ],
        ];
        $app = app('wechat.official_account');
        $app->menu->create($buttons);
    }


}
