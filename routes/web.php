<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Weixin'],function(){
    Route::any('/weixin','IndexController@index')->name('weixin.index.index');
    Route::get('/test','IndexController@test')->name('weixin.index.test');
    Route::get('/create_menu','IndexController@create_menu')->name('weixin.index.create_menu');
    Route::get('/show_menu','IndexController@show_menu')->name('weixin.index.show_menu');
});

Route::group(['namespace' => 'Home','middleware' => ['wechat.oauth:snsapi_userinfo','transfer.user.session']],function(){
    Route::get('/','IndexController@index')->name('home.index.index');
    Route::get('/show','IndexController@show')->name('home.index.show');
});

Route::get('/test','Home\TestController@index');
Route::get('/show_cookie','Home\TestController@show_cookie');
Route::get('/pdf','Home\TestController@pdf');
