<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['remember_token','name','email']);
            $table->string('nickname',128)->nullable(false)->default('')->comment('用户昵称')->after('id');
            $table->string('openid',64)->nullable()->unique()->comment('openid')->after('nickname');
            $table->integer('weixin_id')->unsigned()->nullable(false)->default(0)->comment('所属公众号id')->after('openid');
            $table->integer('recommend_id')->unsigned()->nullable(false)->default(0)->comment('所属推广链接id')->after('weixin_id');
            $table->string('avatar',256)->nullable(false)->default('')->comment('头像地址')->after('recommend_id');
            $table->boolean('sex')->default(1)->comment('1:男生 2:女生')->comment('性别')->after('avatar');
            $table->boolean('subscribe')->default(1)->comment('1:关注 0:未关注')->after('sex');
            $table->integer('book_coin')->unsigned()->default(0)->comment('书币数量')->after('subscribe');
            $table->string('country',128)->default('')->comment('国家')->after('subscribe');
            $table->string('province',128)->default('')->comment('省份')->after('country');
            $table->string('city',128)->default('')->comment('城市')->after('province');
            $table->boolean('is_vip')->default(false)->comment('是否是vip');
            $table->dateTime('vip_expired_at')->nullable()->comment('vip 过期时间');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['nickname','openid','weixin_id','recommend_id','avatar','sex','subscribe','book_coin','country','province','city']);
            $table->string('name');
            $table->rememberToken();
            $table->string('email')->unique();
        });
    }
}
